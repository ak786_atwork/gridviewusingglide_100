package com.example.anil.gridviewusingglide_100;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.util.LruCache;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Stack;


public class MyGridViewAdapter extends BaseAdapter {

    private Activity context ;

    ArrayList<Integer> downloaded = new ArrayList<>();
    ArrayList<Integer> toBeDownloaded = new ArrayList<>();

    Stack<Helper> recentStack = new Stack<>();

    LruCache<Integer,Bitmap> lruCache;

    int count = 0;
    String imageUrls[] = new String[]{ "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSmu_iu_Q4Fl09Gx1G4kGrown2s25vBwVHN8s5Zjpy6x4klnbR9",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSk-PXKszaaBhh8PZW2HizQU1J-VRrt3ex0znp-8-3PiIOL57CuDg",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT7WuV8qMHUQFfAanQ5fTf9YHp-Wvi3l_FpcrjYAX0tQL4lhCaQEQ",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVizgtnwyDyeopApf222ypS2YfEaayyLm_K95u2ode0iUf0dPdSg",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSgF5spTkSTu-naTGO89lwJt4Gy27BJtt3k7PVF4r7PyqCNO-8y",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTYnkW3CJjDJAA8xldNWbvQEuDMUcN0vx-ci7X__tfRYCamdYY",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4dVLuw9zepSY0QFZgJZgh12PLiectY8xT4t-v38HqFKbVN1Fe",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8jcWPnblA5F77UUm8FhtC_254BVmlAtij9r-bdLvfWYj7Bfa_",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQdWyJrJ8taC-_xwfIJDXddXA-g0tDKYT5KYW-YRtNGgmPxQ_5hWw",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRIP4oRK8TRInr2nzX1HXzQx4E3tRhtiRUaEdw8JmqPe_ZMkVsMDw"

    };

    public MyGridViewAdapter(Activity context){
        this.context=context;

        int cacheSize = 4 * 1024 * 1024; // 4MiB
        lruCache = new LruCache<>(cacheSize);
    }

    @Override
    public int getCount() {
        return 500;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null)
        {
            convertView = context.getLayoutInflater().inflate(R.layout.list_item,null);
            viewHolder = new ViewHolder();
            viewHolder.imageView = convertView.findViewById(R.id.imageView);

            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        //viewHolder.imageView.setImageResource(R.mipmap.ic_launcher);
        showLog();
       /* RequestOptions ro = new RequestOptions();
        ro.circleCrop();
        ro.diskCacheStrategy(DiskCacheStrategy.ALL);
        ro.useAnimationPool(true);
        ro.dontTransform();
        ro.encodeFormat(Bitmap.CompressFormat.PNG);
        ro.format(DecodeFormat.PREFER_ARGB_8888);
        ro.priority(Priority.HIGH);

        ro.override(200,200);

        Glide.with(context)
                .load(imageUrls[getRandom()])
                .apply(ro)
                .into(viewHolder.imageView);
*/
        int imageUrl = getRandom();

        if (downloaded.contains(new Integer(imageUrl)))
        {
            viewHolder.imageView.setImageBitmap(lruCache.get(imageUrl));
        }
        else
        {
            Log.d("Adapter", "image not found");
            addToBeDownloaded(imageUrl);
            Helper helper = new Helper();
            helper.imageView = viewHolder.imageView;
            helper.urlPosition = imageUrl;

            recentStack.push(helper);
            Log.d("Adapter", "stack size = "+recentStack.size());
        }

       /*if (lruCache.get(imageUrl) != null)
       {
           viewHolder.imageView.setImageBitmap(lruCache.get(imageUrl));
       }
       else
       {
           addToBeDownloaded(imageUrl);
           (new DownloadImage()).execute(imageUrl);
       }*/

        return convertView;
    }

    //network
    public void makeAPICall()
    {
        if (recentStack.size() >= 1)
        {
            Log.d("API", "api call");
            Helper item = recentStack.pop();


            (new DownloadImage()).execute(item);
            toBeDownloaded.remove(new Integer(item.urlPosition));
        }



    }




    //get random between 0 - 9
    public int getRandom()
    {
        return (int) (Math.random() * 10);
    }

    public void showLog()
    {
        Log.d("Count ","count == " +count++);
    }
    // adding View holder pattern
    static class ViewHolder
    {
        ImageView imageView;
    }

    public void addToBeDownloaded(int urlPosition)
    {
        // yet to  downloaded
        toBeDownloaded.add(urlPosition);
    }

    public void addToDownloaded(int urlPosition)
    {
        // add to downloaded and cache also
        downloaded.add(urlPosition);
        toBeDownloaded.remove(new Integer(urlPosition));
    }
    public void putImageIntoCache(int urlPosition, Bitmap bitmap)
    {
        lruCache.put(urlPosition,bitmap);
    }

    public Bitmap getImageFromCache(int urlPosition)
    {
        return lruCache.get(urlPosition);
    }

    class DownloadImage extends AsyncTask<Helper,Void,Helper>
    {

        @Override
        protected Helper doInBackground(Helper... helpers) {


            Helper helper =  helpers[0];
            int imageURL = helper.urlPosition;
            Bitmap bitmap = null;
            try {
                // Download Image from URL
                InputStream input = new java.net.URL(imageUrls[imageURL]).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
            helper.bitmap = bitmap;
            return helper;
        }

        @Override
        protected void onPostExecute(Helper helper) {
            super.onPostExecute(helper);

            //save to cache
            addToDownloaded(helper.urlPosition);
            putImageIntoCache(helper.urlPosition, helper.bitmap);
            helper.imageView.setImageBitmap(helper.bitmap);
        }
    }


    static class Helper
    {
        int urlPosition;
        Bitmap bitmap;
        ImageView imageView;
    }

}

/*
* package com.example.anil.gridviewusingglide_100;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.util.LruCache;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Stack;

public class MyGridViewAdapter extends BaseAdapter {

    private Activity context ;

    ArrayList<Integer> downloaded = new ArrayList<>();
    ArrayList<Integer> toBeDownloaded = new ArrayList<>();

    Stack<Integer> recentStack = new Stack<>();

    LruCache<Integer,Bitmap> lruCache;

    int count = 0;
    String imageUrls[] = new String[]{ "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSmu_iu_Q4Fl09Gx1G4kGrown2s25vBwVHN8s5Zjpy6x4klnbR9",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSk-PXKszaaBhh8PZW2HizQU1J-VRrt3ex0znp-8-3PiIOL57CuDg",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT7WuV8qMHUQFfAanQ5fTf9YHp-Wvi3l_FpcrjYAX0tQL4lhCaQEQ",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVizgtnwyDyeopApf222ypS2YfEaayyLm_K95u2ode0iUf0dPdSg",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSgF5spTkSTu-naTGO89lwJt4Gy27BJtt3k7PVF4r7PyqCNO-8y",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTYnkW3CJjDJAA8xldNWbvQEuDMUcN0vx-ci7X__tfRYCamdYY",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4dVLuw9zepSY0QFZgJZgh12PLiectY8xT4t-v38HqFKbVN1Fe",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8jcWPnblA5F77UUm8FhtC_254BVmlAtij9r-bdLvfWYj7Bfa_",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQdWyJrJ8taC-_xwfIJDXddXA-g0tDKYT5KYW-YRtNGgmPxQ_5hWw",
           "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRIP4oRK8TRInr2nzX1HXzQx4E3tRhtiRUaEdw8JmqPe_ZMkVsMDw"

    };

    public MyGridViewAdapter(Activity context){
        this.context=context;

        int cacheSize = 4 * 1024 * 1024; // 4MiB
        lruCache = new LruCache<>(cacheSize);
    }

    @Override
    public int getCount() {
        return 500;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null)
        {
            convertView = context.getLayoutInflater().inflate(R.layout.list_item,null);
            viewHolder = new ViewHolder();
            viewHolder.imageView = convertView.findViewById(R.id.imageView);

            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        //viewHolder.imageView.setImageResource(R.mipmap.ic_launcher);
        showLog();
       /* RequestOptions ro = new RequestOptions();
        ro.circleCrop();
        ro.diskCacheStrategy(DiskCacheStrategy.ALL);
        ro.useAnimationPool(true);
        ro.dontTransform();
        ro.encodeFormat(Bitmap.CompressFormat.PNG);
        ro.format(DecodeFormat.PREFER_ARGB_8888);
        ro.priority(Priority.HIGH);

        ro.override(200,200);

        Glide.with(context)
                .load(imageUrls[getRandom()])
                .apply(ro)
                .into(viewHolder.imageView);
*/

/*int imageUrl = getRandom();

       if (downloaded.contains(new Integer(imageUrl)))
               {
               viewHolder.imageView.setImageBitmap(lruCache.get(imageUrl));
               }
               else
               {
               Log.d("Adapter", "image not found");
               addToBeDownloaded(imageUrl);
               recentStack.push(new Integer(imageUrl));
               Log.d("Adapter", "stack size = "+recentStack.size());
               }

       /*if (lruCache.get(imageUrl) != null)
       {
           viewHolder.imageView.setImageBitmap(lruCache.get(imageUrl));
       }
       else
       {
           addToBeDownloaded(imageUrl);
           (new DownloadImage()).execute(imageUrl);
       }

               return convertView;
                       }

//network
public void makeAPICall()
        {
        if (recentStack.size() >= 1)
        {
        Log.d("API", "api call");
        Integer item = recentStack.pop();
        (new DownloadImage()).execute(item);
        toBeDownloaded.remove(item);
        }



        }




//get random between 0 - 9
public int getRandom()
        {
        return (int) (Math.random() * 10);
        }

public void showLog()
        {
        Log.d("Count ","count == " +count++);
        }
// adding View holder pattern
static class ViewHolder
{
    ImageView imageView;
}

    public void addToBeDownloaded(int urlPosition)
    {
        // yet to  downloaded
        toBeDownloaded.add(urlPosition);
    }

    public void addToDownloaded(int urlPosition)
    {
        // add to downloaded and cache also
        downloaded.add(urlPosition);
        toBeDownloaded.remove(new Integer(urlPosition));
    }
    public void putImageIntoCache(int urlPosition, Bitmap bitmap)
    {
        lruCache.put(urlPosition,bitmap);
    }

    public Bitmap getImageFromCache(int urlPosition)
    {
        return lruCache.get(urlPosition);
    }

class DownloadImage extends AsyncTask<Integer,Void,Helper>
{

    @Override
    protected Helper doInBackground(Integer... URL) {
        int imageURL = URL[0];

        Helper helper = new Helper();
        Bitmap bitmap = null;
        try {
            // Download Image from URL
            InputStream input = new java.net.URL(imageUrls[imageURL]).openStream();
            // Decode Bitmap
            bitmap = BitmapFactory.decodeStream(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
        helper.bitmap = bitmap;
        helper.urlPosition = imageURL;
        return helper;
    }

    @Override
    protected void onPostExecute(Helper helper) {
        super.onPostExecute(helper);

        //save to cache
        addToDownloaded(helper.urlPosition);
        putImageIntoCache(helper.urlPosition, helper.bitmap);
    }
}


static class Helper
{
    int urlPosition;
    Bitmap bitmap;
}

}
        */