package com.example.anil.gridviewusingglide_100;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.AbsListView;
import android.widget.GridView;

public class MainActivity extends AppCompatActivity  {

    MyGridViewAdapter myGridViewAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myGridViewAdapter = new MyGridViewAdapter(this);

        GridView gridView = findViewById(R.id.gridview);

        gridView.setAdapter(myGridViewAdapter);

        //initial call

       gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

                Log.d("Main " , "onScrollStateChanged");
                if(scrollState == SCROLL_STATE_IDLE)
                {
                    myGridViewAdapter.makeAPICall();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                Log.d("Main " , "onScroll");
            }
        });

        myGridViewAdapter.makeAPICall();
    }
}
